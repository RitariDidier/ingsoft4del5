package demomvapp.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) throws IOException {
		SpringApplication.run(DemoApplication.class, args);

		Funcionalidad_1 f1 = new Funcionalidad_1();
		Funcionalidad_2 f2 = new Funcionalidad_2();
	}

}
