package demomvapp.demo;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
public class Funcionalidad_2 {
    Document doc = Jsoup.connect("https://si3.bcentral.cl/indicadoressiete/secure/Serie.aspx?gcode=UF&param=RABmAFYAWQB3AGYAaQBuAEkALQAzADUAbgBNAGgAaAAkADUAVwBQAC4AbQBYADAARwBOAGUAYwBjACMAQQBaAHAARgBhAGcAUABTAGUAYwBsAEMAMQA0AE0AawBLAF8AdQBDACQASABzAG0AXwA2AHQAawBvAFcAZwBKAEwAegBzAF8AbgBMAHIAYgBDAC4ARQA3AFUAVwB4AFIAWQBhAEEAOABkAHkAZwAxAEEARAA=").get();
    public Funcionalidad_2() throws IOException {
        Element link = doc.select("#gr_ctl02_Enero").first();

        int nDia = 03; // Dia A buscar
        String mes ="Marzo"; //Con mayuscula la primera




        String dia = "";
        if (nDia <= 9) {
            dia = "0"+(nDia+1);
        } else {
            dia = ""+(nDia+1);
        }
        String valor = obtenerValor(dia,mes);

        calcularVariacion(valor,link);

    }
    private void calcularVariacion(String valor , Element link) {

        valor=formatStringToDouble(valor);
        String link2 =formatStringToDouble(link.text());


        double variacion = Double.parseDouble(valor) - Double.parseDouble(link2);
        System.out.println("Variacion de UF"+variacion);

        calcularPorcentaje(variacion, link2);


    }

    private void calcularPorcentaje(double variacion, String link2) {

        double porcentaje = variacion * 100 / Double.parseDouble(link2);
        System.out.println("El porcentaje de variacion es del "+porcentaje+ "%");

    }


    private String formatStringToDouble(String valor) {
        valor= valor.replace( ".",  "");
        valor=valor.replace( ",",  ".");

        return valor;
    }

    private String obtenerValor(String dia, String mes) {
        Element link2 = doc.select("#gr_ctl"+(dia)+"_"+(mes)).first();
        return link2.text();
    }
}

