package demomvapp.demo;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.Scanner;

public class Funcionalidad_1 {
    Document doc = Jsoup.connect("https://si3.bcentral.cl/indicadoressiete/secure/Serie.aspx?gcode=UF&param=RABmAFYAWQB3AGYAaQBuAEkALQAzADUAbgBNAGgAaAAkADUAVwBQAC4AbQBYADAARwBOAGUAYwBjACMAQQBaAHAARgBhAGcAUABTAGUAYwBsAEMAMQA0AE0AawBLAF8AdQBDACQASABzAG0AXwA2AHQAawBvAFcAZwBKAEwAegBzAF8AbgBMAHIAYgBDAC4ARQA3AFUAVwB4AFIAWQBhAEEAOABkAHkAZwAxAEEARAA=").get();
    public Funcionalidad_1() throws IOException {
        Scanner teclado = new Scanner(System.in);
        System.out.println("Extraccion de UF");
        System.out.print("Mes (ej. Marzo): ");
        String mes = teclado.nextLine();
        System.out.print("Dia: ");
        int dia = teclado.nextInt();

        extraerValor(dia, mes);
    }

    public void extraerValor(int dia, String mes){
        String day;
        dia++;
        if(dia < 10){
            day = "0"+String.valueOf(dia);
        }else{
            day = String.valueOf(dia);
        }

        Elements tabla = doc.select("table.table");

        Element valor = doc.getElementById("gr_ctl"+day+"_"+mes);

        String uf = valor.text();

        dia--;
        System.out.println("El valor de la UF del "+dia+"/"+mes+" es de: "+uf);

    }
}
